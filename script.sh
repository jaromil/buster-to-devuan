#!/bin/bash

cd $(dirname $0)

set -x
set -e

if [ "$1" != "1" ] && [ "$1" != "2" ] ; then
    cat <<EOF >&2
** This script performs "apt migration" from Debian buster to
** Devuan beowulf/chimaera.
** It takes one argument:
** 1 = (stage 1) change the init system (and some little) and reboot
** 2 = (stage 2) upgrade to target distribution and reboot
** 
** Use "DIST=chimaera $0 1" to select chimaera.
** Use "DE=desktop $0 1" to get desktop.
** Use "DIST=chimaera DE=desktop $0 1" to get both.
EOF
    exit 1
fi

: ${DIST:=beowulf} # chimaera

if [ "$DIST" != beowulf ] && [ "$DIST" != chimaera ] ; then
    cat <<EOF >&2
** DIST=$DIST migration is not supported; use DIST=beowulf or chimaera
EOF
    exit 1
fi

if [ "$DE" != "" ] && [ "$DE" != "desktop" ] && [ "$DE" != "xfce" ] && 
       [ "$DE" != "mate" ] && [ "$DE" != "cinnamon" ] && 
       [ "$DE" != "kde" ] ; then
    cat <<EOF >&2
    ** DE=$DE migration is not supported; use DE=desktop or empty
EOF
    exit 1
fi

touch /root/migrate.log
tail -f /root/migrate.log &
exec 1>>/root/migrate.log 2>&1

date -R
echo "arg = $*"

export DEBIAN_FRONTEND=noninteractive
export APT_LISTCHANGES_FRONTEND=none
ENI=/etc/network/interfaces

case "$1" in
    1)	# FIRST STAGE
	#
	# prepare $ENI for proper interface naming
	# download and install the Devuan keyring
	# install sysvinit-core, to replace systemd at next boot
	# disable dbus for the sysvinit boot, and remove some stuff
	# set up automatic second stage at boot

	eni_snippet() {
	    cat << EOF >> $ENI
#-- Snippet to bring up $1 - the $2 named wired interface
auto $1
iface $1 inet dhcp
#--
EOF
	}    
	IFACE="$(ip route | awk '$1 == "default" { print $5; exit; }')"
	grep -qw eth0 $ENI || eni_snippet eth0 $DIST
	[ "$IFACE" = "eth0" ] || [[ "$IFACE" != e* ]] || \
	    eni_snippet $IFACE buster

	wget https://pkgmaster.devuan.org/devuan/pool/main/d/devuan-keyring/devuan-keyring_2017.10.03_all.deb
	dpkg -i devuan-keyring_2017.10.03_all.deb

	apt-get update -y -q

	apt-get -y install sysvinit-core

	update-rc.d dbus disable
	apt-get -y purge avahi-daemon sane-utils

	touch /etc/rc.local
	chmod a+x /etc/rc.local
	mv /etc/rc.local /etc/rc.local.ORIG
	cat <<EOF > /etc/rc.local
#!/bin/sh
# Perform stage 2 of Devuan $DIST migration
/usr/sbin/sshd -D
cd $(pwd)
DIST=$DIST DE=$DE ./$(basename $0) 2
EOF
	chmod a+x /etc/rc.local

	# Clean out unused packages and reboot
	apt-get -y autoremove --purge
	echo
	echo "Stage 1 completed!"
	echo
	echo "Now try to reboot using:"
	echo "init 6"
	echo " or"
	echo "systemctl reboot"
	echo
	echo "Next reboot will perform a dist-upgrade"
	echo "Please be patient and wait it completes with another reboot"
	echo "Login via ssh and use 'watch ps af' to monitor progress"
	;;

    2)	# SECOND STAGE

	# restore to "normal" boot
	# set up the new sources.list to use $DIST
	# update, dist-upgrade and some fixup
	# optionally install the xfce desktop environment

	[ -f /etc/rc.local.ORIG ] && mv /etc/rc.local.ORIG /etc/rc.local

	mv /etc/apt/sources.list /etc/apt/sources.list.ORIG
	cat << END > /etc/apt/sources.list
deb http://deb.devuan.org/merged $DIST main contrib non-free
deb http://deb.devuan.org/merged $DIST-updates main contrib non-free
END

	apt-get -y update
	apt-get -y dist-upgrade
	apt-get -y install libelogind0
	apt-get -y purge dbus-user-session libnss-systemd systemd plymouth

	case "$DE" in
	    desktop)
		apt-get -y install task-xfce-desktop
		;;
	    xfce|mate|cinnamon|kde)
		apt-get -y install task-$DE-desktop
		;;
	    *)
		:
		;;
	esac

	# Clean out unused packages and reboot
	apt-get -y autoremove --purge
	reboot
	;;
esac
